//
//  Helpers.h
//  skillbox
//
//  Created by EVGENY TABOLIN on 14.09.2020.
//  Copyright © 2020 EVGENY TABOLIN. All rights reserved.
//

#ifndef Helpers_h
#define Helpers_h

#include <cmath>

int square(int a, int b)
{
    return pow(a, b);
}


#endif /* Helpers_h */
