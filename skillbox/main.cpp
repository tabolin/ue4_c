//
//  main.cpp
//  skillbox
//
//  Created by EVGENY TABOLIN on 14.09.2020.
//  Copyright © 2020 EVGENY TABOLIN. All rights reserved.
//

#include <iostream>

template<class T>
T sum2(T a, T b)
{
    return  a + b;
}

class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector(): x(0), y(0), z(0)
    {}
    Vector(double _x,double _y,double _z): x(_x), y(_y), z(_z)
    {}
    void print()
    {
        std::cout << x << " " << y << " " << z << std::endl;
    }
    
    double vectorLength()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z,2));
    }
};

int main()
{
    Vector v(2,4,4);
    v.print();
    std::cout << v.vectorLength();
}
